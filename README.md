# Eric's Game 2
![Eric's Game 2](./screenshots/EricsGame2Snap1.jpg) 

## Description
"Eric's Game 2" was originally developed as a sequel to Eric's Castle Volume 1.  However, due time constraints (school) as well as other personal reasons (me getting a PC and discovering QBasic) this unnamed computer game was never actually finished.  Like the original Eric's Castle, "Eric's Game 2" is a text based adventure game.  This program was developed on a computer running Mac OS 7.5.3 using a program called [World Builder](http://en.wikipedia.org/wiki/World_Builder).  In "Eric's Game 2" the player first searches for, and then explores an old tower.  In addition to the text based and menu controls similar to those in Eric's Castle Volume 1, Eric's Game 2 contains on-screen buttons that the player can click on with a mouse, which have the same effect as typing out commands.  Although it is unfinished, it should be playable up until the point I stopped working on it.  The screenshots below should give you a better idea about the game.

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Screenshots
Introduction Screen:

![Eric's Game 2](./screenshots/EricsGame2Snap1.jpg)

At the Bay:

![Eric's Game 2](./screenshots/EricsGame2Snap2.jpg)

Outside the Tower:

![Eric's Game 2](./screenshots/EricsGame2Snap3.jpg)

Inside the Tower:

![Eric's Game 2](./screenshots/EricsGame2Snap4.jpg)

## Instructions
This is an image file of the floppy disc the program was stored on.  It won't run on a modern Macintosh, but if you've got some old hardware lying around feel free to download it.

## History
I'm really not sure when I started working on this (according to the file, it was created in 1987 and I know that's not right), but my best guess is either late 1997 or early 1998.  Based on the file's last modified timestamp, it looks like I stopped working on it on April 11th, 1998 but it should be noted that I have no idea how accurate that is.

